// <-- ENGLISH IMPOSED -->

tol = 1e-6;
L = 3;

// check against dwh2invLgt0

z = poly(0,"z");
H = [2 + 1/z + 1/z^2 ; 4/z^3];
SYSH = tf2ss(H);
SYSH.dt = "d";
SYSW = syslin("d",[],[],[],eye(3,3));
SYSG = dwh2invLgt0([SYSH;1],SYSW,L);
SYSG = SYSG(:,1:2);

L = 1/z^L;
SYSL = tf2ss(L);
SYSL.dt = "d";
SYSK = wkf(SYSL,SYSH,1,eye(2,2));

if dh2norm(SYSG - SYSK)>tol then pause; end;