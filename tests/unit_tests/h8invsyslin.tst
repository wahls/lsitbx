// <-- ENGLISH IMPOSED -->

rand("seed",123);
warning("off");

n = 4;
p = 3;
q = 4;
L = 3;
tol = 1e-6;
test_tol = 1e-8;

/// create random systems

SYSHd = syslin("d",rand(n,n),rand(n,p),rand(q,n),rand(q,p));
SYSHc = syslin("c",rand(n,n),rand(n,p),rand(q,n),rand(q,p));

/// compare input-output behaviour with direct calls

SYSG1 = h8invsyslin(SYSHd,struct("verbose",%f));
SYSG2 = dh8invLeq0(SYSHd,0.01,test_tol,%f);
if norm(SYSG1.A-SYSG2.A)+norm(SYSG1.B-SYSG2.B)+...
  norm(SYSG1.C-SYSG2.C)+(SYSG1.D-SYSG2.D)>0 then pause; end;

SYSG1 = h8invsyslin(SYSHc,struct("verbose",%f));
SYSG2 = ch8invLeq0(SYSHc,0.01,test_tol,%f);
if norm(SYSG1.A-SYSG2.A)+norm(SYSG1.B-SYSG2.B)+...
  norm(SYSG1.C-SYSG2.C)+(SYSG1.D-SYSG2.D)>0 then pause; end;

SYSG1 = h8invsyslin(SYSHd,struct("tol",0.1,"test_tol",test_tol,"verbose",%f));
SYSG2 = dh8invLeq0(SYSHd,0.1,test_tol,%f);
if norm(SYSG1.A-SYSG2.A)+norm(SYSG1.B-SYSG2.B)+...
  norm(SYSG1.C-SYSG2.C)+(SYSG1.D-SYSG2.D)>0 then pause; end;

SYSG1 = h8invsyslin(SYSHc,struct("tol",0.1,"test_tol",test_tol,"verbose",%f));
SYSG2 = ch8invLeq0(SYSHc,0.1,test_tol,%f);
if norm(SYSG1.A-SYSG2.A)+norm(SYSG1.B-SYSG2.B)+...
  norm(SYSG1.C-SYSG2.C)+(SYSG1.D-SYSG2.D)>0 then pause; end;

/// check whether transposition works as expected

SYSG1 = h8invsyslin(SYSHd,struct("verbose",%f));
SYSG2 = h8invsyslin(SYSHd',struct("verbose",%f))';
if norm(SYSG1.A-SYSG2.A)+norm(SYSG1.B-SYSG2.B)+...
  norm(SYSG1.C-SYSG2.C)+(SYSG1.D-SYSG2.D)>0 then pause; end;