// <-- ENGLISH IMPOSED -->

rand("seed",123);

for p = 1:5 do
  for k = 1:200 do
    // test real discrete-time case

    // test stable case
    A = rand(5,5);
    A = rand()/max(abs(spec(A)))*A;
    B = rand(5,p);
    K = cplxstabil(A,B,"d");
    if max(abs(spec(A+B*K)))>=1 then pause; end;

    // test SISO case
    A = 10;
    B = 2;
    K = cplxstabil(A,B,"d");
    if max(abs(spec(A+B*K)))>=1 then pause; end;

    // test unstable case
    A = rand(5,5);
    A = (1+rand())/max(abs(spec(A)))*A;
    B = rand(5,p);
    K = cplxstabil(A,B,"d");
    if max(abs(spec(A+B*K)))>=1 then pause; end;

    // test unstable+uncontrollable case
    A1 = rand(2,2);
    A1 = rand()/max(abs(spec(A1)))*A1;
    A2 = rand(3,3);
    A2 = (1+rand())/max(abs(spec(A2)))*A2;
    A = [A1 zeros(2,3) ; rand(3,2) A2];
    B = [zeros(2,p) ; rand(3,p)];
    K = cplxstabil(A,B,"d");
    if max(abs(spec(A+B*K)))>=1 then pause; end;

    // test complex discrete-time case

    // test stable case
    A = rand(5,5) + %i*rand(5,5);
    A = rand()/max(abs(spec(A)))*A;
    B = rand(5,p) + %i*rand(5,p);
    K = cplxstabil(A,B,"d");
    if max(abs(spec(A+B*K)))>=1 then pause; end;

    // test SISO case
    A = 10-14*%i;
    B = 2+1*%i;
    K = cplxstabil(A,B,"d");
    if max(abs(spec(A+B*K)))>=1 then pause; end;

    // test unstable case
    A = rand(5,5) + %i*rand(5,5);
    A = (1+rand())/max(abs(spec(A)))*A;
    B = rand(5,p) + %i*rand(5,p);
    K = cplxstabil(A,B,"d");
    if max(abs(spec(A+B*K)))>=1 then pause; end;

    // test unstable+uncontrollable case
    A1 = rand(2,2) + %i*rand(2,2);
    A1 = rand()/max(abs(spec(A1)))*A1;
    A2 = rand(3,3) + %i*rand(3,3);
    A2 = (1+rand())/max(abs(spec(A2)))*A2;
    A = [A1 zeros(2,3) ; rand(3,2)+%i*rand(3,2) A2];
    B = [zeros(2,p) ; rand(3,p)+%i*rand(3,p)];
    K = cplxstabil(A,B,"d");
    if max(abs(spec(A+B*K)))>=1 then pause; end;

    // test real continuous-time case

    // test stable case
    A = rand(5,5);
    A = A-(max(real(spec(A)))+rand())*eye(5,5);
    B = rand(5,p);
    K = cplxstabil(A,B,"c");
    if max(real(spec(A+B*K)))>=0 then pause; end;

    // test SISO case
    A = 10;
    B = 2;
    K = cplxstabil(A,B,"c");
    if max(real(spec(A+B*K)))>=0 then pause; end;

    // test unstable case
    A = rand(5,5);
    A = A-(max(real(spec(A)))-rand())*eye(5,5);
    B = rand(5,p);
    K = cplxstabil(A,B,"c");
    if max(real(spec(A+B*K)))>=0 then pause; end;

    // test unstable+uncontrollable case
    A1 = rand(2,2);
    A1 = A1-(max(real(spec(A1)))+rand())*eye(2,2);
    A2 = rand(3,3);
    A2 = A2-(max(real(spec(A2)))-rand())*eye(3,3);
    A = [A1 zeros(2,3) ; rand(3,2) A2];
    B = [zeros(2,p) ; rand(3,p)];
    K = cplxstabil(A,B,"c");
    if max(real(spec(A+B*K)))>=0 then pause; end;

    // test complex continous-time case

    // test stable case
    A = rand(5,5) + %i*rand(5,5);
    A = A-(max(real(spec(A)))+rand())*eye(5,5);
    B = rand(5,p) + %i*rand(5,p);
    K = cplxstabil(A,B,"c");
    if max(real(spec(A+B*K)))>=0 then pause; end;

    // test SISO case
    A = 10-14*%i;
    B = 2+1*%i;
    K = cplxstabil(A,B,"c");
    if max(real(spec(A+B*K)))>=0 then pause; end;

    // test unstable case
    A = rand(5,5) + %i*rand(5,5);
    A = A-(max(real(spec(A)))-rand())*eye(5,5);
    B = rand(5,p) + %i*rand(5,p);
    K = cplxstabil(A,B,"c");
    if max(real(spec(A+B*K)))>=1 then pause; end;

    // test unstable+uncontrollable case
    A1 = rand(2,2) + %i*rand(2,2);
    A1 = A1-(max(real(spec(A1)))+rand())*eye(2,2);
    A2 = rand(3,3) + %i*rand(3,3);
    A2 = A2-(max(real(spec(A2)))-rand())*eye(3,3);
    A = [A1 zeros(2,3) ; rand(3,2)+%i*rand(3,2) A2];
    B = [zeros(2,p) ; rand(3,p)+%i*rand(3,p)];
    K = cplxstabil(A,B,"c");
    if max(real(spec(A+B*K)))>=0 then pause; end;
  end
end