// <-- ENGLISH IMPOSED -->

rand("seed",123);

n = 4;
p = 3;
q = 4;
L = 3;
tol = 1e-6;

/// create random system and weight

SYSH = syslin("d",rand(n,n),rand(n,p),rand(q,n),rand(q,p));
SYSW = syslin("d",rand(n,n),rand(q,n),rand(n,q),rand(q,q));
SYSW.A = 0.9 * SYSW.A / max(abs(spec(SYSW.A)));

/// compare input-output behaviour with direct calls

SYSG1 = h2invsyslin(SYSH);
SYSG2 = dwh2invLeq0(SYSH,syslin("d",[],[],[],eye(q,q)));
if norm(SYSG1.A-SYSG2.A)+norm(SYSG1.B-SYSG2.B)+...
  norm(SYSG1.C-SYSG2.C)+(SYSG1.D-SYSG2.D)>0 then pause; end;

[SYSG1,SYSA1,SYSB1] = h2invsyslin(SYSH);
[SYSG2,SYSA2,SYSB2] = dwh2invLeq0(SYSH,syslin("d",[],[],[],eye(q,q)));
if (norm(SYSG1.A-SYSG2.A)+norm(SYSG1.B-SYSG2.B)+...
  norm(SYSG1.C-SYSG2.C)+(SYSG1.D-SYSG2.D)>0) ...
  | (norm(SYSA1.A-SYSA2.A)+norm(SYSA1.B-SYSA2.B)+...
  norm(SYSA1.C-SYSA2.C)+(SYSA1.D-SYSA2.D)>0) ...
  | (norm(SYSB1.A-SYSB2.A)+norm(SYSB1.B-SYSB2.B)+...
  norm(SYSB1.C-SYSB2.C)+norm(SYSB1.D-SYSB2.D)>0) ...
  then pause; end;

SYSG1 = h2invsyslin(SYSH,struct("delay",L));
SYSG2 = dwh2invLgt0(SYSH,syslin("d",[],[],[],eye(q,q)),L);
if norm(SYSG1.A-SYSG2.A)+norm(SYSG1.B-SYSG2.B)+...
  norm(SYSG1.C-SYSG2.C)+(SYSG1.D-SYSG2.D)>0 then pause; end;

[SYSG1,SYSA1,SYSB1] = h2invsyslin(SYSH,struct("delay",L));
[SYSG2,SYSA2,SYSB2] = dwh2invLgt0(SYSH,syslin("d",[],[],[],eye(q,q)),L);
if (norm(SYSG1.A-SYSG2.A)+norm(SYSG1.B-SYSG2.B)+...
  norm(SYSG1.C-SYSG2.C)+(SYSG1.D-SYSG2.D)>0) ...
  | (norm(SYSA1.A-SYSA2.A)+norm(SYSA1.B-SYSA2.B)+...
  norm(SYSA1.C-SYSA2.C)+(SYSA1.D-SYSA2.D)>0) ...
  | (norm(SYSB1.A-SYSB2.A)+norm(SYSB1.B-SYSB2.B)+...
  norm(SYSB1.C-SYSB2.C)+norm(SYSB1.D-SYSB2.D)>0) ...
  then pause; end;

SYSG1 = h2invsyslin(SYSH,struct("delay",L,"weight",SYSW));
SYSG2 = dwh2invLgt0(SYSH,SYSW,L);
if norm(SYSG1.A-SYSG2.A)+norm(SYSG1.B-SYSG2.B)+...
  norm(SYSG1.C-SYSG2.C)+(SYSG1.D-SYSG2.D)>0 then pause; end;

[SYSG1,SYSA1,SYSB1] = h2invsyslin(SYSH,struct("delay",L,"weight",SYSW));
[SYSG2,SYSA2,SYSB2] = dwh2invLgt0(SYSH,SYSW,L);
if (norm(SYSG1.A-SYSG2.A)+norm(SYSG1.B-SYSG2.B)+...
  norm(SYSG1.C-SYSG2.C)+(SYSG1.D-SYSG2.D)>0) ...
  | (norm(SYSA1.A-SYSA2.A)+norm(SYSA1.B-SYSA2.B)+...
  norm(SYSA1.C-SYSA2.C)+(SYSA1.D-SYSA2.D)>0) ...
  | (norm(SYSB1.A-SYSB2.A)+norm(SYSB1.B-SYSB2.B)+...
  norm(SYSB1.C-SYSB2.C)+norm(SYSB1.D-SYSB2.D)>0) ...
  then pause; end;

/// check whether transposition works as expected

SYSG1 = h2invsyslin(SYSH);
SYSG2 = h2invsyslin(SYSH')';
if norm(SYSG1.A-SYSG2.A)+norm(SYSG1.B-SYSG2.B)+...
  norm(SYSG1.C-SYSG2.C)+(SYSG1.D-SYSG2.D)>0 then pause; end;

[SYSG1,SYSA1,SYSB1] = h2invsyslin(SYSH,struct("delay",L,"weight",SYSW));
[SYSG2,SYSA2,SYSB2] = h2invsyslin(SYSH',struct("delay",L,"weight",SYSW'));
SYSG2 = SYSG2';
SYSA2 = SYSA2';
SYSB2 = SYSB2';
if (norm(SYSG1.A-SYSG2.A)+norm(SYSG1.B-SYSG2.B)+...
  norm(SYSG1.C-SYSG2.C)+(SYSG1.D-SYSG2.D)>0) ...
  | (norm(SYSA1.A-SYSA2.A)+norm(SYSA1.B-SYSA2.B)+...
  norm(SYSA1.C-SYSA2.C)+(SYSA1.D-SYSA2.D)>0) ...
  | (norm(SYSB1.A-SYSB2.A)+norm(SYSB1.B-SYSB2.B)+...
  norm(SYSB1.C-SYSB2.C)+norm(SYSB1.D-SYSB2.D)>0) ...
  then pause; end;

/// test handling of singular D matrices

SYSG1 = h2invsyslin(SYSH);
SYSG2 = h2invsyslin(add_delay(SYSH,L),struct("delay",L));
if dh2norm(SYSG1-SYSG2)>tol then pause; end;