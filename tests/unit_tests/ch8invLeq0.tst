// <-- ENGLISH IMPOSED -->

rand("seed",123);
warning("off");
tol = 1e-3;
test_tol = 1e-8;

// test inversion property

// real case
SYSH = syslin("c",rand(4,4),rand(4,2),rand(4,4),rand(4,2));
SYSG = ch8invLeq0(SYSH,tol,test_tol,%f);
if h_norm(SYSG*SYSH-eye(),tol)>tol then pause; end;

// complex case
SYSH = syslin("c",rand(4,4)+%i*rand(4,4),rand(4,2)+%i*rand(4,2), ...
  rand(4,4)+%i*rand(4,4),rand(4,2)+%i*rand(4,2));
SYSG = ch8invLeq0(SYSH,tol,test_tol,%f);
if h_norm(SYSG*SYSH-eye(),tol)>tol then pause; end;

// test optimality of inverses

s = poly(0,"s");

// first test

al = 1.2;
H = [1 0 0 0 ; al/s 1 0 0 ; 0 al/s 1 0 ; 0 0 al/s 1 ; 0 0 0 al/s];
G = [1 0 0 0 0 ; -al/s 1 0 0 0 ; (-al/s)^2 (-al/s) 1 0 0 ; ...
  (-al/s)^3 (-al/s)^2 (-al/s) 1 0 ; ];
H = horner(H,(1+s)/(1-s));
G = horner(G,(1+s)/(1-s));

SYSH = tf2ss(H);
SYSH.dt = "c";
opts = struct("tol",tol,"verbose",%f);
SYSG = h8invsyslin(SYSH,opts);

// approximate norms, h_norm seems to have problems
J1 = 0;
J2 = 0;
omegas = 0.001:0.001:2*%pi;
for om = omegas do
  z = exp(%i*om);
  s = (1+z)/(1-z);
  J1 = max(J1,norm(SYSG.D+SYSG.C*inv(s*eye(SYSG.A)-SYSG.A)*SYSG.B));
  J2 = max(J2,norm(horner(G,s)));
end

if abs(J2-J1)>tol then pause; end;

// second test - [1 1] also is a H-infinity optimal inverse of H in continuous
// time

s = poly(0,"s");
H = [1/s ; 1-1/s];
SYSH = tf2ss(H);
SYSH.dt = "c";
SYSG = h8invsyslin(SYSH,opts);

// h_norm has problems
J1 = 0;
omegas = 0.001:0.001:2*%pi;
for om = omegas do
  z = exp(%i*om);
  s = (1+z)/(1-z);
  J1 = max(J1,norm(SYSG.D+SYSG.C*inv(s*eye(SYSG.A)-SYSG.A)*SYSG.B));
end

if abs(sqrt(2)-J1)>tol then pause; end;
