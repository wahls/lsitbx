// <-- ENGLISH IMPOSED -->

tol = 1e-4;

// check example from Ionescu and Weiss: "On computing the stabilizing solution
// of the discrete-time Riccati equation"

A = [0 1 ; 0 -1];
B = [1 0 ; 2 1];
Q = [-4 -4 ; -4 7]/11;
L = [3 1 ; -1 7];
R = [9 3 ; 3 1];

[X,F] = dare(A,B,R,L,Q);

if norm(X - [-1.4021 13.0569 ; 13.0569 -125.6365])>tol then pause; end;

if norm(F - [-0.9405 11.0098 ; 1.7829 -19.6090])>tol then pause; end;

// check some random examples

rand("seed",123);
q = 3;

// real case
for p = 1:q do
  for k = 1:100 do
    // create random problem
    A = rand(q,q,"normal");
    B = rand(q,p,"normal");
    R = rand(p,p,"normal");
    R = R*R';
    L = 0.01*rand(q,p,"normal");
    Q = rand(q,q,"normal");
    Q = Q*Q';

    // solve DARE
    [X,F] = dare(A,B,R,L,Q);

    // test residual
    if norm(A'*X*A-X-(L+A'*X*B)*inv(R+B'*X*B)*(B'*X*A+L')+Q)>tol ...
      then pause; end;

    // test that solution is stabilizing
    if max(abs(spec(A+B*F)))>1 then pause; end;
  end
end

// complex case
for p = 1:q do
  for k = 1:100 do
    // create random problem
    A = rand(q,q,"normal") + %i*rand(q,q,"normal");
    B = rand(q,p,"normal") + %i*rand(q,p,"normal");
    R = rand(p,p,"normal") + %i*rand(p,p,"normal");
    R = R*R';
    L = 0.01*(rand(q,p,"normal") + %i*rand(q,p,"normal"));
    Q = rand(q,q,"normal") + %i*rand(q,q,"normal");
    Q = Q*Q';

    // solve DARE
    [X,F] = dare(A,B,R,L,Q);

    // test residual
    if norm(A'*X*A-X-(L+A'*X*B)*inv(R+B'*X*B)*(B'*X*A+L')+Q)>tol ...
      then pause; end;

    // test that solution is stabilizing
    if max(abs(spec(A+B*F)))>1 then pause; end;
  end
end