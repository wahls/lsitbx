// <-- ENGLISH IMPOSED -->

rand("seed",123);

tol = 1e-6;

// create random system with two first rank-deficient taps
SYS = syslin("d",0.01*(rand(3,3)+%i*rand(3,3)),rand(3,3)+%i*rand(3,3), ...
  rand(4,3)+%i*rand(4,3),(rand(4,1)+%i*rand(4,1))*(rand(1,3)+%i*rand(1,3)));
delay = eye(3,3)*syslin("d",0,1,1,0);
SYS = SYS*delay + (rand(4,1)+%i*rand(4,1))*(rand(1,3)+%i*rand(1,3));

// compute factorization
[SMP,A,Atilde,deg] = smpafact(SYS,2);

// check that the degree is correct
if deg~=2 then pause; end;

// check that the factorization holds
if dh2norm(SMP*A-SYS)>tol then pause; end;
if dh2norm(SYS*Atilde-SMP*delay*delay)>tol then pause; end;

// check that Atilde is the deg-delay inverse of A
if dh2norm(A*Atilde-delay*delay)>tol then pause; end;

// check that A and Atilde are FIR of degree deg
if (norm(A.C*A.A^(deg-1)*A.B)<tol)|(norm(A.A^deg)>tol) then pause; end;
if (norm(Atilde.C*Atilde.A^(deg-1)*Atilde.B)<tol)|(norm(Atilde.A^deg)>tol) ...
  then pause; end;

// check that SMP is strictly minimum phase
if rank(SMP.D)<3 then pause; end;
INVSYS = h2invsyslin(SMP);
if max(abs(spec(INVSYS.A)))>=1 then pause; end;

// check that A and Atilde are all-pass
max_err = 0;
for th=0:0.01:2*%pi do
  z = exp(th*%i);
  val = (A.D+A.C*inv(z*eye(A.A)-A.A)*A.B)* ...
    (Atilde.D+Atilde.C*inv(z*eye(Atilde.A)-Atilde.A)*Atilde.B);
  max_err = max(max_err,norm(val-eye(val)/z^deg));
end
if max_err>tol then pause; end;