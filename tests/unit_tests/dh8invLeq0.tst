// <-- ENGLISH IMPOSED -->

rand("seed",123);
warning("off");

tol = 1e-2;
test_tol = 1e-8;

/// test inversion property

// real case
SYSH = syslin("d",rand(4,4),rand(4,2),rand(4,4),rand(4,2));
SYSH.A = rand()/max(abs(spec(SYSH.A)))*SYSH.A;
U = ones(size(SYSH,2),100);
SYSG = dh8invLeq0(SYSH,tol,test_tol,%f);
CHK = SYSG*SYSH;
Uhat = dsimul(CHK,U);
if norm(Uhat-U)>tol then pause; end;
CHK = CHK - eye(CHK);
CHK.dt = "d";
if dhnorm(CHK,tol)>tol then pause; end;

// complex case
SYSH = syslin("d",rand(4,4)+%i*rand(4,4),rand(4,2)+%i*rand(4,2), ...
  rand(4,4)+%i*rand(4,4),rand(4,2)+%i*rand(4,2));
SYSG = dh8invLeq0(SYSH,tol,test_tol,%f);
if dhnorm(SYSG*SYSH - eye(),tol)>tol then pause; end;

/// test optimality of inverses

// First test - it can be shown that G is a H-infinity optimal inverse of H

z = poly(0,"z");
al = 1.2;
H = [1 0 0 0 ; al/z 1 0 0 ; 0 al/z 1 0 ; 0 0 al/z 1 ; 0 0 0 al/z];
G = [1 0 0 0 0 ; -al/z 1 0 0 0 ; (-al/z)^2 (-al/z) 1 0 0 ; ...
  (-al/z)^3 (-al/z)^2 (-al/z) 1 0];

SYSH = tf2ss(H);
SYSH.dt = "d";
SYSG = dh8invLeq0(SYSH,tol,test_tol,%f);

// dhnorm has some problems here
J1 = 0;
J2 = 0;
omegas = 0.001:0.001:2*%pi;
for om = omegas do
  z = exp(%i*om);
  J1 = max(J1,norm(horner(G,z)));
  J2 = max(J2,norm(SYSG.D+SYSG.C*inv(z*eye(SYSG.A)-SYSG.A)*SYSG.B));
end

if abs(J2-J1)>tol then pause; end;

// Second test - it can be shown that [1 1] is a H-infinity optimal inverse of H

z = poly(0,"z");
H = [1/z ; 1-1/z];
SYSH = tf2ss(H);
SYSH.dt = "d";
SYSG = dh8invLeq0(SYSH,tol,test_tol,%f);

// dhnorm has problems with SYSG
J1 = 0;
omegas = 0.001:0.001:2*%pi;
for om = omegas do
  z = exp(%i*om);
  J1 = max(J1,norm(SYSG.D+SYSG.C*inv(z*eye(SYSG.A)-SYSG.A)*SYSG.B));
end

if abs(sqrt(2)-J1)>tol then pause; end;

/// test correct handling of initial conditions

SYSH = syslin("d",rand(4,4),rand(4,2),rand(4,4),rand(4,2));
SYSH.A = rand()*SYSH.A/max(abs(spec(SYSH.A)));
SYSH.X0 = rand(4,1);
SYSW = syslin("d",rand(4,4),rand(4,4),rand(4,4),rand(4,4));
SYSG = dh8invLeq0(SYSH,tol,test_tol,%f);
u = rand(2,100);
y = dsimul(SYSH,u);
uhat = dsimul(SYSG,y);
if norm(u-uhat)>tol then pause; end;
