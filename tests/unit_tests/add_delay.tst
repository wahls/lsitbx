// <-- ENGLISH IMPOSED -->

rand("seed",123);

tol = 1e-6;

z = poly(0,"z");
SYSH = syslin("d",rand(4,4),rand(4,2),rand(3,4),rand(3,2));
SYSHd = add_delay(SYSH,3);
SYSHd2 = tf2ss(ss2tf(SYSH)/z^3);
if dh2norm(SYSHd-SYSHd2)>tol then pause; end;