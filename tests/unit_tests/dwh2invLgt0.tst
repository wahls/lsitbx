// <-- ENGLISH IMPOSED -->

warning("off");

tol = 1e-4;

/// check unweighted inversion

rand("seed",123);
SYSH = syslin("d",rand(4,4),rand(4,2),rand(4,4),rand(4,2));
SYSHc = syslin("d",rand(4,4)+%i*rand(4,4),rand(4,2)+%i*rand(4,2), ...
  rand(4,4)+%i*rand(4,4),rand(4,2)+%i*rand(4,2));

// test inversion property
SYSG = dwh2invLgt0(SYSH,syslin("d",[],[],[],eye(4,4)),20);
if dh2norm(SYSG*SYSH-add_delay(syslin("d",[],[],[],eye(2,2)),20))>tol ...
  then pause; end;

// check that the inverse is stable
if max(abs(spec(SYSG.A)))>1 then pause; end;

// check optimality by comparison with an approximate solution obtained from the
// Wiener-Kalman filter
SYSG2 = wkf(add_delay(syslin("d",[],[],[],eye(2,2)),20),SYSH,eye(2,2), ...
  1e-9*eye(4,4));
if dh2norm(SYSG-SYSG2)>tol then pause; end;

// check that the weighted norm is minimal
// (inverses converge towards pseudoinverse when the delay grows large)
[SYSR0c,SYSR0ac] = parapinv(SYSH);
if abs(sqrt(dh2norm(SYSR0c)^2+dh2norm(SYSR0ac)^2)-dh2norm(SYSG))>tol ...
  then pause; end;

// repeat tests with complex plant
SYSGc = dwh2invLgt0(SYSHc,syslin("d",[],[],[],eye(4,4)),20);
if dh2norm(SYSGc*SYSHc-add_delay(syslin("d",[],[],[],eye(2,2)),20))>tol ...
  then pause; end;
if max(abs(spec(SYSGc.A)))>1 then pause; end;
SYSG2c = wkf(add_delay(syslin("d",[],[],[],eye(2,2)),20),SYSHc,eye(2,2), ...
  1e-9*eye(4,4));
if dh2norm(SYSGc-SYSG2c)>tol then pause; end;
[SYSR0c,SYSR0ac] = parapinv(SYSHc);
if abs(sqrt(dh2norm(SYSR0c)^2+dh2norm(SYSR0ac)^2)-dh2norm(SYSGc))>tol ...
  then pause; end;

/// check weighted inversion

SYSH = syslin("d",0.5,1,[-0.25;-0.5;-0.75],[-0.5;-1;-1.5]);
SYSW = syslin("d",0,1,[1;0;0],[0;0.1;0.1]);
[SYSG,SYSA,SYSB] = dwh2invLgt0(SYSH,SYSW,1);

// test inversion property
SYSz  = syslin("d",0,1,1,0);
if dh2norm(SYSG*SYSH-SYSz)>tol then pause; end;

// the that weighted norm is minimal
if dhnorm(SYSG*SYSW)>tol then pause; end;

// check the parametrization
if max(abs(spec(SYSA.A)))>1 then pause; end;
if max(abs(spec(SYSB.A)))>1 then pause; end;
if dh2norm(SYSA*SYSH-SYSz)>tol then pause; end;
if dh2norm(SYSA*SYSW)>tol then pause; end;
if dh2norm(SYSB*SYSH)>tol then pause; end;
if dh2norm(SYSB*SYSW)>tol then pause; end;
z = rand(1,1,"normal")+%i*rand(1,1,"normal");
rk = rank(SYSB.D+SYSB.C*inv(z*eye()-SYSB.A)*SYSB.B);	// normal rank of SYSB
if rk<1 then pause; end;

/// test correct handling of initial conditions

SYSH = syslin("d",rand(4,4),rand(4,2),rand(4,4),rand(4,2));
SYSH.A = rand()*SYSH.A/max(abs(spec(SYSH.A)));
SYSH.X0 = rand(4,1);
SYSW = syslin("d",rand(4,4),rand(4,4),rand(4,4),rand(4,4));
SYSG = dwh2invLgt0(SYSH,SYSW,10);
u = rand(2,100);
y = dsimul(SYSH,u);
uhat = dsimul(SYSG,y);
if norm(u(:,1:$-10)-uhat(:,11:$))>tol then pause; end;