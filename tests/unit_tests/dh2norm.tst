// <-- ENGLISH IMPOSED -->

rand("seed",123);

tol = 1e-6;

// generate random FIR system
p = 2; q = 3; N = 5; n = N*p;
A = [zeros(p,n) ; eye(n-p,n-p) zeros(n-p,p)];
B = [eye(p,p) ; zeros(n-p,p)];
C = rand(q,n);
D = rand(q,p);
SYSH = minreal(syslin("d",A,B,C,D));

if abs(dh2norm(SYSH)-norm([C D],"fro"))>tol then pause; end;

// repeat test with complex system
C = rand(q,n) + %i*rand(q,n);
D = rand(q,p) + %i*rand(rand(q,p));;
SYSH = syslin("d",A,B,C,D);

if abs(dh2norm(SYSH)-norm([C D],"fro"))>tol then pause; end;
