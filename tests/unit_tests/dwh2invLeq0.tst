// <-- ENGLISH IMPOSED -->

warning("off");

tol = 1e-4;

/// check unweighted inversion

rand("seed",123);
SYSH = syslin("d",rand(4,4),rand(4,2),rand(4,4),rand(4,2));
SYSHc = syslin("d",rand(4,4)+%i*rand(4,4),rand(4,2)+%i*rand(4,2), ...
  rand(4,4)+%i*rand(4,4),rand(4,2)+%i*rand(4,2));

// test inversion property
SYSG = dwh2invLeq0(SYSH,syslin("d",[],[],[],eye(4,4)));
if dh2norm(SYSG*SYSH-eye(2,2))>tol then pause; end;

// check that the inverse is stable
if max(abs(spec(SYSG.A)))>1 then pause; end;

// repeat tests for complex plant
SYSGc = dwh2invLeq0(SYSHc,syslin("d",[],[],[],eye(4,4)));
if dh2norm(SYSGc*SYSHc-eye(2,2))>tol then pause; end;
if max(abs(spec(SYSGc.A)))>1 then pause; end;

// check optimality by comparison with an approximate solution obtained from the
// Wiener-Kalman filter
SYSG2 = wkf(syslin("d",[],[],[],eye(2,2)),SYSH,eye(2,2),1e-9*eye(4,4));
if dh2norm(SYSG-SYSG2)>tol then pause; end;

// repeat for complex plant
SYSG2c = wkf(syslin("d",[],[],[],eye(2,2)),SYSHc,eye(2,2),1e-9*eye(4,4));
if dh2norm(SYSGc-SYSG2c)>tol then pause; end;

// check optimality for a known plant
z = poly(0,"z");
al = 1.2;
H = [1 0 0 0 ; al/z 1 0 0 ; 0 al/z 1 0 ; 0 0 al/z 1 ; 0 0 0 al/z];
G = [1 0 0 0 0 ; -al/z 1 0 0 0 ; (-al/z)^2 (-al/z) 1 0 0 ; ...
  (-al/z)^3 (-al/z)^2 (-al/z) 1 0];
SYSH = tf2ss(H);
SYSH.dt = "d";
SYSG = dwh2invLeq0(SYSH,syslin("d",[],[],[],eye(5,5)));
SYSG2 = tf2ss(G);
SYSG2.dt = "d";
if dh2norm(SYSG-SYSG2)>tol then pause; end;

/// check weighted inversion

SYSH = syslin("d",0.5,1,[-0.25;-0.5;-0.75],[-0.5;-1;-1.5]);
SYSW = syslin("d",0,1,[1;0;0],[0;0.1;0.1]);
[SYSG,SYSA,SYSB] = dwh2invLeq0(SYSH,SYSW);

// test inversion property
SYSId  = syslin("d",0,0,0,1);
if dh2norm(SYSG*SYSH-SYSId)>tol then pause; end;

// check that the that weighted norm is minimal
if dhnorm(SYSG*SYSW)>tol then pause; end;

// check the parametrization
if max(abs(spec(SYSA.A)))>1 then pause; end;
if max(abs(spec(SYSB.A)))>1 then pause; end;
if dh2norm(SYSA*SYSH-SYSId)>tol then pause; end;
if dh2norm(SYSA*SYSW)>tol then pause; end;
if dh2norm(SYSB*SYSH)>tol then pause; end;
if dh2norm(SYSB*SYSW)>tol then pause; end;
z = rand(1,1,"normal")+%i*rand(1,1,"normal");
rk = rank(SYSB.D+SYSB.C*inv(z*eye()-SYSB.A)*SYSB.B);	// normal rank of SYSB
if rk<1 then pause; end;

/// test correct handling of initial conditions

SYSH = syslin("d",rand(4,4),rand(4,2),rand(4,4),rand(4,2));
SYSH.A = rand()*SYSH.A/max(abs(spec(SYSH.A)));
SYSH.X0 = rand(4,1);
SYSW = syslin("d",rand(4,4),rand(4,4),rand(4,4),rand(4,4));
SYSG = dwh2invLeq0(SYSH,SYSW);
u = rand(2,100);
y = dsimul(SYSH,u);
uhat = dsimul(SYSG,y);
if norm(u-uhat)>tol then pause; end;