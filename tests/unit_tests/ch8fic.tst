// <-- ENGLISH IMPOSED -->

warning("off");
test_tol = 1e-8;

// example 1

tol = 1e-6;

A = [0.85 0.13 1.09 ; 1.19 -0.22 -1.35 ; 2.08 -0.50 -0.55];
B = [-0.53 -0.43 -1.34 -0.15 -1.00 ; 0.18 0.83 0.35 -0.34 1.43 ; ...
  0.66 0.58 -1.19 0.94 -0.28];
B1 = B(:,1:3);
B2 = B(:,4:5);
C = [0.32 -0.46 0.32 ; 1.84 1.57 1.30];
D1 = zeros(2,3);
D2 = [-1.05 0.67 ; -0.14 0.01];

ub = 10;
N = ceil(log2(ub/tol));
[F1,F2,ub] = ch8fic(A,B1,B2,C,D1,D2,0,ub,N,test_tol,%f);

// check the upper bound
if abs(0.793305415875238-ub)>tol then pause; end;

// check that the upper bounds actually is true
J = 0;
omegas = 0.001:0.001:2*%pi;
for om = omegas do
  z = exp(%i*om);
  s = (1+z)/(1-z);
  J = max(J,norm((D1+D2*F2)+(C+D2*F1)*pinv(s*eye()-A-B2*F1)*(B1+B2*F2)));
end
if J>1.001*ub then pause; end;

// check closed-loop stability
if max(real(spec(A+B2*F1)))>=0 then pause; end;

// example 2

tol = 1e-0;

A = [-2 -40 -400 -4000 ; 0 -2 -40 -400 ; 0 0 -2 -40 ; 0 0 0 -2];
B1 = [0 40 400 4000 ; 0 0 40 400 ; 0 0 0 40 ; 0 0 0 0];
B2 = [40000 ; 4000 ; 400 ; 40];
C = [-1 -10 -100 -1000 ; 0 -1 -10 -100 ; 0 0 -1 -10 ; 0 0 0 -1 ; 0 0 0 0];
D1 = -C;
D2 = [10000 ; 1000 ; 100 ; 100 ; 1];

ub = 2000;
N = ceil(log2(ub/tol));
[F1,F2,ub] = ch8fic(A,B1,B2,C,D1,D2,0,ub,N,test_tol,%f);

// check the upper bound
if abs(1010.100970202018-ub)>tol then pause; end;

// check that the upper bounds actually is true
J = 0;
omegas = 0.001:0.001:2*%pi;
for om = omegas do
  z = exp(%i*om);
  s = (1+z)/(1-z);
  J = max(J,norm((D1+D2*F2)+(C+D2*F1)*pinv(s*eye()-A-B2*F1)*(B1+B2*F2)));
end
if J>1.001*ub then pause; end;

// check closed-loop stability
if max(real(spec(A+B2*F1)))>=0 then pause; end;

// example 3

tol = 1e-6;

A = [0 1 0 ; 0 0 1 ; 0 -1 1];
B1 = [0 ; 1 ; 0];
B2 = [0 ; 0 ; 1];
C = [1 -1 1 ; 1 1 0];
D1 = [0 ; 1];
D2 = [1 ; 0];

ub = 10;
N = ceil(log2(ub/tol));
[F1,F2,ub] = ch8fic(A,B1,B2,C,D1,D2,0,ub,N,test_tol,%f);

// check the upper bound
if abs(sqrt(5)-ub)>tol then pause; end;

// check that the upper bounds actually is true
J = 0;
omegas = 0.001:0.001:2*%pi;
for om = omegas do
  z = exp(%i*om);
  s = (1+z)/(1-z);
  J = max(J,norm((D1+D2*F2)+(C+D2*F1)*pinv(s*eye()-A-B2*F1)*(B1+B2*F2)));
end
if J>1.001*ub then pause; end;

// check closed-loop stability
if max(real(spec(A+B2*F1)))>=0 then pause; end;
