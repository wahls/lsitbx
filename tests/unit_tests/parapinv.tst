// <-- ENGLISH IMPOSED -->

// check example from Chai et. al: "Frame-Theory-Based Analysis and Design
// of Oversampled Filter Banks: Direct Computational Method"

tol = 1e-4;

z = poly(0,"z");
E = [(0.4208*z+0.06665)/(z-0.02509) 0.4875*z/(z-0.02509) ; ...
  (0.2452*z-0.2452)/(z+0.5095) 0 ; ...
  (0.4208*z+0.06665)/(z-0.02509) -0.4875*z/(z-0.02509)];
SYSE = tf2ss(E);
SYSE.dt = "d";

[SYSR0c,SYSR0ac] = parapinv(SYSE);

RES = minss(tf2ss(ss2tf(SYSR0c) + horner(ss2tf(SYSR0ac),1/z) - ...
  inv(horner(E,1/z)'*E)*horner(E,1/z)'));
RES.dt = "d";

if dh2norm(RES)>tol then pause; end;