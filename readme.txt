CONTENT

This archive contains the Linear System Inversion Toolbox for Scilab (see
<http://lsitbx.origo.ethz.ch> for more information).

INSTALLATION

1. Extract the contents of this archive into some directory, e.g.,
  /home/john/lsitbx

2. Run Scilab (version 5.2 or higher)

3. Build the toolbox by executing the command
  <exec /home/john/lsitbx/builder.sce> (replace the directory with your chosen
  path; do not type the "<" and ">")

USAGE

Each time before the toolbox is used it has to be loaded with the command
<exec /home/john/lsitbx/loader.sce> (replace the directory with your chosen
path). Run the Scilab command <help "Linear System Inversion Toolbox"> to get
more information.