z = poly(0,"z");		// define symbolic z variable
H = [1/z ; 1-1/z]		// define 2x1 LTI system in z-domain
SYSH = tf2ss(H);		// transform into state-space system
SYSH.dt = "d";			// mark as discrete-time
SYSG = h2invsyslin(SYSH);	// compute inverse with min. H2 norm
G = ss2tf(SYSG);		// transform inverse into z-domain
clean(G)			// show transfer function of inverse
clean(G*H)			// should be one
SYSG = h8invsyslin(SYSH);	// compute inverse with min. H-infinity norm
G = ss2tf(SYSG);		// transform inverse into z-domain
clean(G)			// show transfer function of inverse
clean(G*H)			// should be one