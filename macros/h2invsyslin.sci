function [SYSG,varargout] = h2invsyslin(SYSH,opts)
//
// Purpose:
//
//	Interface to the various inversion routines with H2 criterion.
//
// Description:
//
//	The routine is intended as a interface to the inversion routines
//	dwh2invLeq0 and dwh2invLgt0. The main tasks are to evaluate the user
//	inputs, to check validity of the input parameters (dwh2invLeq0 and
//	dwh2invLgt0 do not check inputs) and to call the correct routine.
//
// Authors:
//
//	Sander WAHLS
//

  // check number of outputs
  lhs = argn(1);
  if lhs>3 then
    error("Too many return values expected.");
  end

  // check number of inputs
  rhs = argn(2);
  if rhs<1 then
    error("Not enough arguments.");
  end

  // no additional outputs by default
  varargout = list();

  // define local opts to avoid namespace mixup 
  if rhs<2 then
    opts = struct();
  end

  // check SYSH
  if typeof(SYSH)~="state-space" then
    error("SYSH is no state-space system.");
  end
  if typeof(SYSH.D)~="constant" then
    error("SYSH.D is no constant.");
  end
  if SYSH.dt~="d" then
    error("Currently only discrete-time systems are supported " ...
      +"(SYSH.dt~=""d"").");
  end

  // convert right inversion problem into left inversion problem if necessary
  [q,p] = size(SYSH);
  transposed = q<p;
  if transposed==%T then
    SYSH = SYSH';
    [q,p] = size(SYSH);
  end

  // check opts
  if ~isstruct(opts) then
    error("opts is no struct.");
  end

  // check SYSW
  try
    SYSW = opts.weight;
  catch
    SYSW = syslin("d",[],[],[],eye(q,q));
  end
  if transposed==%T then
    SYSW = SYSW';
  end
  if typeof(SYSW)~="state-space" then
    error("opts.weight is no state-space system.");
  end
  if typeof(SYSW.D)~="constant" then
    error("opts.weight.D is no constant.");
  end
  if SYSW.dt~="d" then
    error("Currently only discrete-time systems are supported " ...
      +"(opts.weight.dt~=""d"").");
  end
  if q~=size(SYSW,1) then
    error("Dimensions of SYSH and opts.weight are incompatible.");
  end
  if max(abs(spec(SYSW.A)))>=1 then
    error("opts.weight is not stable.");
  end

  // check delay L
  try
    L = opts.delay;
  catch
    L = 0;
  end
  try
    isreal(L);
  catch
    error("opts.delay is not real.");
  end
  if L<0 then
    error("opts.delay is negative.");
  end
  if L~=round(L) then
    error("opts.delay is no integer.");
  end

  // check if the system to be inverted has a rank deficient D matrix
  rank_deficient_D = rank(SYSH.D)<p;
  if rank_deficient_D==%T then
    if L==0 then
      error("SYSH has no delay-free inverses because SYSH.D is singular.");
    else
      try
	// factorize the system as SYSH=SMP*A where SMP has a zero-delay inverse
	// SYSG, and A is FIR inner with degree deg<=L; the inverse then is
	// Atilde*SMPI, where SMPI is the optimal (L-deg)-delay inverse of SMP
	warning("SYSH.D is singular. Trying to factorize SYSH.");
	[SMP,A,Atilde,deg] = smpafact(SYSH,L);
	warning(sprintf("The resulting %i-delay inverse may be suboptimal " ...
	  +"(but it performs at least as good as any %i-delay inverse).", ...
	  L,L-deg));
	SYSH = SMP;
      catch
	error(sprintf("Factorization algorithm failed. Is SYSH stably " ...
	  +"%i-delay invertible? Error message was ""%s"".",L,lasterror()));
      end
      L = L-deg;
    end
  else
    deg = 0;
  end

  // call apropiate left inversion routine
  if q==p then
    SYSG = invsyslin(SYSH);
    SYSG = add_delay(SYSG,L);
    if max(abs(spec(SYSG.A)))>=1 then
      error("Inverse is unstable.");
    end
  elseif L==0 then
    if lhs<=1 then
      SYSG = dwh2invLeq0(SYSH,SYSW);
    else
      [SYSG,SYSA,SYSB] = dwh2invLeq0(SYSH,SYSW);
      varargout = list(SYSA,SYSB);
    end
  elseif L>0 then
    if lhs<=1 then
      SYSG = dwh2invLgt0(SYSH,SYSW,L);
    else
      [SYSG,SYSA,SYSB] = dwh2invLgt0(SYSH,SYSW,L);
      varargout = list(SYSA,SYSB);
    end
  end

  // add delayed inverse of the all-pass factor if SYSH has been factorized
  if rank_deficient_D==%T then
    SYSG = Atilde*SYSG;
  end

  // revert transposition if necessary
  if transposed==%T then
    SYSG = SYSG';
    if lhs>1 then
      varargout = list(SYSA',SYSB');
    end
  end

endfunction