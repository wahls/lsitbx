function SYSG = h8invsyslin(SYSH,opts)
//
// Purpose:
//
//	Interface to the various inversion routines with H-infinity criterion.
//
// Description:
//
//	The routine is intended as a interface to the inversion routines
//	ch8invLeq0 and dh8invLeq0. The main tasks are to evaluate the user
//	inputs, to check validity of the input parameters (ch8invLeq0 and
//	dh8invLeq0 do not check inputs) and to call the correct routine.
//
// Authors:
//
//	Sander WAHLS
//

  // check number of inputs
  rhs = argn(2);
  if rhs<1 then
    error("Not enough arguments.");
  end

  // define local opts to avoid namespace mixup 
  if rhs<2 then
    opts = struct();
  end

  // check opts
  if ~isstruct(opts) then
    error("opts is no struct.");
  end

  // check SYSH
  if typeof(SYSH)~="state-space" then
    error("SYSH is no state-space system.");
  end
  if typeof(SYSH.D)~="constant" then
    error("SYSH.D is no constant.");
  end
  if (SYSH.dt~="d")&(SYSH.dt~="c") then
    error("Time domain of SYSH is unknown (SYSH.dt~=""d"" and " ...
      +"SYSH.dt~=""c"").");
  end

  if rank(SYSH.D)<min(size(SYSH.D)) then
    error("SYSH.D is rank deficient.");
  end

  // check tolerance
  try
    tol = opts.tol;
  catch
    tol = 1e-2;
  end
  try
    isreal(tol)
  catch
    error("opts.tol is not real.");
  end
  if tol<=0 then
    error("opts.tol is not positive.");
  end

  // check tolerance used in numerical tests
  try
    test_tol = opts.test_tol;
  catch
    test_tol = 1e-8;
  end
  try
    isreal(test_tol)
  catch
    error("opts.test_tol is not real.");
  end
  if test_tol<0 then
    error("opts.test_tol is negative.");
  end

  // check verbose
  try
    verbose = opts.verbose;
  catch
    verbose = %t;
  end
  if typeof(verbose)~="boolean" then
    error("verbose is no boolean.");
  end
  if size(verbose,"*")~=1 then
    error("verbose is a vector or matrix.");
  end

  // convert right inversion problem into left inversion problem if necessary
  [q,p] = size(SYSH.D);
  transposed = q<p;
  if transposed==%T then
    SYSH = SYSH';
  end

  // call apropiate left inversion routine
  if (q==p) then
    SYSG = invsyslin(SYSH);
    if SYSH.dt=="d" then
      if max(abs(spec(SYSG.A)))>=1 then
	error("Inverse is unstable.");  
      end
    else
      if max(real(spec(SYSG.A)))>=0 then
	error("Inverse is unstable.");
      end
    end
  elseif SYSH.dt=="d" then
    SYSG = dh8invLeq0(SYSH,tol,test_tol,verbose);
  else
    SYSG = ch8invLeq0(SYSH,tol,test_tol,verbose);
  end

  // revert transpose if necessary
  if transposed==%T then
    SYSG = SYSG';  
  end

endfunction
