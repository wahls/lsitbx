function [F1,F2,ub] = ch8fic(A,E,B,C,D2,D1,lb,ub,N,test_tol,verbose)
//
// Purpose:
//
//	Solves the continuous-time H-infinity full information control problem.
//
// Description:
//
//	We consider the stabilizable continuous-time state-space system 
//	x'(t)=A*x(t)+B*u(t)+E*w(t), y(t)=C*x(t)+D1*u(t)+D2*w(t), where x(t)
//	is the state, u(t) is the control input and w(t) is a disturbance. We
//	assume that the subsystem from u to y has no zeros on the imaginary
//	axis and that D1 is injective. The routine tries to find a stabilizing
//	controller u(t)=F1*x(t)+F2*w(t) such that the H-infinity norm of the
//	closed-loop system is approximately infimal. The scalars lb and ub are
//	initial lower and upper bounds on the infimal norm. The algorithm works
//	iteratively and N is a natural which indicates how many steps should be
//	carried out. The scalar test_tol can be used to regulate some internal
//	tolerances. The boolean parameter verbose determines whether the routine
//	should output status messages while it is running.
//
// Algorithm:
//
//	See Theorem 3.1 in A. Stoorvogel, "The H-infinity control problem:
//	a state space approach", Prentice-Hall, 1992
//
// Authors:
//
//	Sander WAHLS
//

  // extract matrix sizes
  [p,l] = size(D2);
  [p,m] = size(D1);
  n = size(A,1);

  // display status message if requested
  if verbose then
    printf("\nTesting upper bound:\n");
  end

  // check that upper bound is larger than lower bound
  if lb>=ub then
    error("Lower bound has to be stricly less than upper bound.");
  end

  // initially test upper bound, increase it if neccessary
  [ok,P] = ch8fic_test_gam(A,E,B,C,D2,D1,ub,test_tol);
  if ok==%f then
    warning("Upper bound is too low. Trying 2^20*ub with 20 additional " ...
      +"iterations.");
    ub = 2^20*ub;
    N = N+20;	// add 20 iteration to achieve same precision as before
  end

  gam_ok = -1;	// this variable will contain the last valid gamma such that a
		// solution the the full information control problem existed;
		// -1 if there wasn't one

  // display status message if requested
  if verbose then
    printf("\nStarting gamma iteration (%d iterations):\n", N);
  end

  // gamma iteration
  for k = 1:N do

    // set gamma to the center of the test interval
    gam = 0.5*(lb+ub);

    // determine whether gamma is valid
    [ok,P] = ch8fic_test_gam(A,E,B,C,D2,D1,gam,test_tol);

    // update test interval
    if ok==%t then
      ub = gam;
      gam_ok = gam;
      P_ok = P;
    else
      lb = gam;
    end

  end

  // none of the tested gammas was valid => upper bound too low
  if gam_ok==-1 then
    error("Upper bound still too low.");
  end

  // compute optimal controller
  TMP = -inv(D1'*D1);
  F1 = TMP*(D1'*C+B'*P_ok);
  F2 = TMP*D1'*D2;

endfunction

function [ok,P] = ch8fic_test_gam(A,E,B,C,D2,D1,gam,test_tol)

  // initialize paramters
  ok = %f;
  P = [];

  // scale factor for the tolerances in the tests
  scale = max(max(abs([A,E,B;C,D2,D1])));

  // test whether D2'*(I-D1*inv(D1'*D1)*D1')*D2 - gam^2*I<0
  chk1 = min(real(spec(D2'*(eye(p,p)-D1*inv(D1'*D1)*D1')*D2-gam^2*eye(l,l))));
  if chk1>=test_tol*scale then
    if verbose then
      printf("  %e: infeasible\t(D2''*(I-D1*inv(D1''*D1)*D1'')*D2>=gam^2*I," ...
	+" min(real(spec(...)))=%e)\n",gam,chk1);
    end
    return;
  end

  // prepare coefficients of the riccati equation
  TMP = inv([D1'*D1 D1'*D2 ; D2'*D1 D2'*D2-gam^2*eye(l,l)]);
  Acirc = A-[B E]*TMP*[D1 D2]'*C;
  Gcirc = [B E]*TMP*[B E]';
  Qcirc = C'*(eye(p,p)-[D1 D2]*TMP*[D1 D2]')*C;

  // try to solve the riccati equation
  try
    P = riccati(Acirc,Gcirc,Qcirc,"c");
    // err=Acirc'*P+P*Acirc-P*[B E]*TMP*[B E]'*P+C'*C-C'*[D1 D2]*TMP*[D1 D2]'*C;
    // printf("error in riccati eq: %f\n",norm(err));
  catch
    if verbose then
      printf("  %e: infeasible\t(Riccati solver failed: ""%s"")\n",gam, ...
	lasterror());
    end
    return;
  end

  // test whether P is hermitian
  chk2 = norm(P-P',1);
  if chk2>test_tol*scale then
    if verbose then
      printf("  %e: infeasible\t(P not symmetric, ||P-P''||=%e)\n",gam,chk2);
    end
    return;
  end

  // test whether P>=0
  chk3 = min(real(spec(P)));
  if chk3<-test_tol*scale then
    if verbose then
      printf("  %e: infeasible\t(P<0, min(real(spec(P)))=%e)\n",gam,chk3);
    end
    return;
  end   

  // test whether Acl is stable
  chk4 = max(real(spec(A-[B E]*TMP*([B E]'*P+[D1 D2]'*C))));
  if chk4>=0 then
    if verbose then
      printf("  %e: infeasible\t(Acl unstable, max(real(spec(Acl)))=%e)\n", ...
	gam,chk4);
    end
    return;
  end

  ok = %t;	// all tests passed

  // display status message if requested
  if verbose then
    printf("  %e: feasible\n",gam);
  end

endfunction