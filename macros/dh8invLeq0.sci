function SYSG = dh8invLeq0(SYSH,tol,test_tol,verbose)
//
// Purpose:
//
//	Computes stable discrete-time left inverses with approximately infimal
//	H-infinity norm.
//
// Description:
//
//	We consider the discrete-time state-space system SYSH with transfer
//	function H(z). We assume that H(z) is tall with full normal rank and
//	that all zeros of SYSH are located inside the open unit disc. The
//	algorithm computes a stable discrete-time state-space system SYSG with
//	transfer function G(z) such that G(z)H(z)=I and the H-infinity norm of
//	G(z) is close to infimal. The scalar tol>0 regulates how close the norm
//	of G(z) should be to the infimal value. Note: A larger value means a
//	longer run-time. Too small values may result in numerical problems. The
//	scalar test_tol regulates the tolerance in some internal tests. The
//	boolean verbose determines whether status messages should be displayed
//	while the algorithm is running.
//
// Algorithm:
//
//	The problem is reformulated as a full information control problem
//	which is then solved with dh8fic(). See Li, L. & Gu, G., "Design of
//	optimal zero-forcing precoders for MIMO channels via optimal full
//	information control", IEEE Trans. Signal Process., 2005, vol. 53,
//	pp. 3238-3246
//
// Authors:
//
//	Sander WAHLS
//

  // the used algorithm is for right inversion, so we transpose SYS
  SYSH = SYSH';
  
  // extract system matrices and their sizes
  [A,B,C,D] = abcd(SYSH);
  [q,p] = size(D);
  n = size(A,1);

  // construct Dplus and Dperp
  Dplus = pinv(D);
  [U,S1,V] = svd(eye(p,p)-Dplus*D);
  Dperp = U(:,1:p-q)';

  // compute A0 and B0
  A0 = A-B*Dplus*C;
  B0 = B*Dperp';

  // estimate bounds for the full information problem
  // see http://arxiv.org/abs/0803.0428 for the lower bound
  if (A~=[])&(B~=[])&(C~=[])&(D~=[]) then
    Z = zeros(D);
    lb = 1/min(svd([D Z Z ; C*B D Z ; C*A*B C*B D]));
  else
    lb = 0;
  end
  F = cplxstabil(A0,B0,"d");
  ub = max((1+tol)*lb,(1+tol)*dhnorm(syslin("c",A0+B0*F,B*Dplus, ...
    Dperp'*F-Dplus*C,Dplus))+2*tol);
  N = ceil(log2((ub - lb)/tol));
  
  // solve full information problem
  [Kopt,Qopt,nrm] = dh8fic(A0,B*Dplus,B0,-Dplus*C,Dplus,Dperp',lb,ub,N, ...
    test_tol,verbose);

  // construct optimal right inverse for transpose_syslin(SYS)
  Ainv = A0+B0*Kopt;
  Binv = B*Dplus+B0*Qopt;
  Cinv = Dperp'*Kopt-Dplus*C;
  Dinv = Dplus+Dperp'*Qopt;
  x0inv = -SYSH.x0;
  SYSG = syslin("d",Ainv,Binv,Cinv,Dinv,x0inv);

  // transpose again to obtain optimal left inverse
  SYSG = SYSG';
  
endfunction
