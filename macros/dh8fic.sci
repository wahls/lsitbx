function [F1,F2,ub] = dh8fic(A,E,B,C,D2,D1,lb,ub,N,test_tol,verbose)
//
// Purpose:
//
//	Solves the discrete-time H-infinity full information control problem.
//
// Description:
//
//	We consider the stabilizable discrete-time state-space system
//	x(t+1)=A*x(t)+B*u(t)+E*w(t), y(t)=C*x(t)+D1*u(t)+D2*w(t), where x(t)
//	is the state, u(t) is the control input and w(t) is a disturbance. We
//	assume that the subsystem from u to y has no zeros on the unit
//	circle and that D1 is injective. The routine tries to find a stabilizing
//	controller u(t)=F1*x(t)+F2*w(t) such that the H-infinity norm of the
//	closed-loop system is approximately infimal. The scalars lb and ub are
//	initial lower and upper bounds on the infimal norm. The algorithm works
//	iteratively and N is a natural which indicates how many steps should be
//	carried out. The scalar test_tol can be used to regulate some internal
//	tolerances. The boolean parameter verbose determines whether the routine
//	should output status messages while it is running.
//
// Algorithm:
//
//	See Theorem 9.2 in A. Stoorvogel, "The H-infinity control problem: a
//	state space approach", Prentice-Hall, 1992
//
// Authors:
//
//	Sander WAHLS
//

  // extract matrix sizes
  [p,l] = size(D2);
  [p,m] = size(D1);
  n = size(A,1);

  // initialize some matrices
  D10 = D1;
  D20 = D2;
  C0 = C;

  // display status message if requested
  if verbose then
    printf("\nTesting upper bound:\n");
  end

  // check that upper bound is larger than lower bound
  if lb>=ub then
    error("Lower bound has to be stricly less than upper bound.");
  end

  // initially test upper bound, increase it if neccessary
  C = C0/ub;
  D1 = D10/ub;
  D2 = D20/ub;
  [ok,P] = dh8fic_test_gam(A,E,B,C,D2,D1,ub,test_tol);
  if ok==%f then
    warning("Upper bound is too low. Trying 2^20*ub with 20 additional " ...
      +"iterations.")
    ub = 2^20*ub;
    N = N+20;	// add 20 iteration to achieve same precision as before
  end

  gam_ok = -1;	// this variable will contain the last valid gamma such that a
		// solution the the full information control problem existed;
		// -1 if there wasn't one

  // display status message if requested
  if verbose then
    printf("\nStarting gamma iteration (%d iterations):\n", N);
  end

  // gamma iteration
  for k = 1:N do

    // set gamma to the center of the test interval
    gam = 0.5*(lb+ub);

    // rescale matrices (test_gam always tests the case gamma=1)
    C = C0/gam;
    D1 = D10/gam;
    D2 = D20/gam;

    // determine whether gamma is valid
    [ok,P] = dh8fic_test_gam(A,E,B,C,D2,D1,gam,test_tol);

    // update test interval
    if ok==%t then
      ub = gam;
      gam_ok = gam;
      P_ok = P;
    else
      lb = gam;
    end

  end

  // none of the tested gammas was valid => upper bound too low
  if gam_ok==-1 then
    error("Upper bound still too low.");
  end

  // compute optimal controller
  TMP = inv(D10'*D10/gam^2+B'*P_ok*B);
  F1 = -TMP*(B'*P_ok*A+D10'*C0/gam^2);
  F2 = -TMP*(B'*P_ok*E+D10'*D20/gam^2);

endfunction

function [ok,P] = dh8fic_test_gam(A,E,B,C,D2,D1,gam,test_tol)

  // initialize some variables
  ok = %f;
  P = [];

  // scale factor for the tolerances in the tests
  scale = max(max(abs([A,B,E;C,D2,D1])));

  // try to solve the riccati equation
  try
    P = dare(A,[B E],[D1'*D1 D1'*D2 ; D2'*D1 D2'*D2-eye(l,l)],C'*[D1 D2],C'*C);
  catch
    if verbose then
      printf("  %e: infeasible\t(Riccati solver failed: ""%s"")\n",gam, ...
	lasterror());
    end
    return;
  end

  // test whether P>=0
  chk5 = min(real(spec(P)));
  if chk5<-test_tol*scale then
    if verbose then
      printf("  %e: infeasible\t(P<0, min(real(spec(P)))=%e)\n",gam,chk5);
    end
    return;
  end      

  // test whether V<=0
  V = D1'*D1+B'*P*B;
  chk1 = min(real(spec(V)));
  if chk1<=-test_tol*scale then
    if verbose then
      printf("  %e: infeasible\t(V<=0, min(real(spec(V)))=%e)\n",gam,chk1);
    end
    return;
  end

  // test whether R<=0
  chk2 = min(real(spec(eye(l,l)-D2'*D2-E'*P*E+(E'*P*B+D2'*D1)*inv(V)* ...
    (B'*P*E+D1'*D2))));
  if chk2<=-test_tol*scale then
    if verbose then
      printf("  %e: infeasible\t(R<=0, min(real(spec(R)))=%e)\n",gam,chk2);
    end
    return;
  end

  // test whether Acl is stable
  GP = [D1'*D1 D1'*D2 ; D2'*D1 D2'*D2-eye(l,l)]+[B E]'*P*[B E];
  Acl = A-[B E]*inv(GP)*[B'*P*A+D1'*C ; E'*P*A+D2'*C];
  chk3 = max(abs(spec(Acl)));
  if chk3>=1 then
    if verbose then
      printf("  %e: infeasible\t(Acl instable, max(abs(spec(Acl)))=%e)\n", ...
	gam,chk3);
    end
    return;
  end

  ok = %t;	// all tests passed

  // display status message if requested
  if verbose then
    printf("  %e: feasible\n",gam);
  end

endfunction