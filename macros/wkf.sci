function SYSK = wkf(SYSL,SYSH,Q,R)
//
// Purpose:
//
//	Computes the Wiener-Kalman filter.
//
// Description:
//
//	We consider the stable discrete-time state-space systems SYSL and SYSH
//	with transfer functions L(Z) and H(z). Q and R are positive-definite
//	hermitian matrices such that R*H(z)*Q is well-defined. The routine finds
//	the causal and stable K(z) such that the H2 norm
//	||[(L(z)-K(z)H(z))*sqrt(Q), -K(z)*sqrt(R)]|| is minimal. It returns a
//	state-space realization of K(z).
//
// Algorithm:
//
//	See Theorem 13.2.1 in Hassibi, Sayed, Kailath: "Indefinite-Quadratic
//	Estimation and Control", SIAM, 1999
//
// Authors:
//
//	Sander WAHLS
//

  // check number of inputs
  rhs = argn(2);
  if rhs<4 then
    error("Not enough arguments.");
  end

  // check SYSL
  if typeof(SYSL)~="state-space" then
    error("SYSL is no state-space system.");
  end
  if typeof(SYSL.D)~="constant" then
    error("SYSL.D is no constant.");
  end
  if SYSL.dt~="d" then
    error("SYSL is no discrete-time system (SYSL.dt~=""d"").");
  end

  // check SYSH
  if typeof(SYSH)~="state-space" then
    error("SYSH is no state-space system.");
  end
  if typeof(SYSH.D)~="constant" then
    error("SYSH.D is no constant.");
  end
  if SYSH.dt~="d" then
    error("SYSH is no discrete-time system (SYSH.dt~=""d"").");
  end

  // check Q and R
  if typeof(Q)~="constant" then
    error("Q is no matrix.");
  end
  if typeof(R)~="constant" then
    error("R is no matrix.");
  end
  if or(clean(R-R')~=0) then
    error("R is not hermitian.");
  end
  if or(clean(Q-Q')~=0) then
    error("Q is not hermitian.");
  end

  // extract dimensions of SYSL and SYSH
  [qL,pL] = size(SYSL);
  [qH,pH] = size(SYSH);

  // check conformity of dimensions
  if pL~=pH then
    error("Numbers of inputs differ between SYSL and SYSH.");
  end
  if or(size(Q)~=[pH,pH]) then
    error("Q has wrong dimensions.");
  end
  if or(size(R)~=[qH,qH]) then
    error("R has wrong dimensions.");
  end

  // create state-space model with joint state-space
  TMP = [SYSL' SYSH'];
  
  // compute DL,CL,DH,CL,A,B s.t. H(z)=DH+CH*inv(z*eye(A)-A)*B
  // and L(z)=DL+CL*inv(z*eye(A)-A)*B
  DL = (TMP.D*[eye(qL,qL) ; zeros(qH,qL)])';
  CL = (TMP.B*[eye(qL,qL) ; zeros(qH,qL)])';
  DH = (TMP.D*[zeros(qL,qH) ; eye(qH,qH)])';
  CH = (TMP.B*[zeros(qL,qH) ; eye(qH,qH)])';
  B = TMP.C';
  A = TMP.A';

  // compute F,G,H,L s.t. L(z)=L*inv(z*eye(F)-F)*G
  // and H(z)/z=H*inv(z*eye(F)-F)*G
  [n,n] = size(TMP.A);
  F = [A B ; zeros(pH,n) zeros(pH,pH)];
  G = [zeros(n,pH) ; eye(pH,pH)];
  H = [CH DH];
  L = [CL DL];

  // solve Riccati equation
  P = dare(F',H',R,zeros(H'),G*Q*G');

  // create optimal estimator
  Re = R+H*P*H';
  invRe = inv(Re);
  Kp = F*P*H'*invRe;
  SYSK = syslin("d",F-Kp*H,Kp,L*(eye(P)-P*H'*invRe*H),L*P*H'*invRe);

endfunction