function [SMP,A,Atilde,deg] = smpafact(SYSH,L)
//
// Purpose:
//
//	Factorizes a discrete-time state-space system into a strictly
//	minimum-phase system and a inner FIR system.
//
// Description:
//
//	We consider the discrete-time state-space system SYSH with transfer
//	function H(z) and the decision delay L. We assume that H(z) has full
//	column normal rank and that the zeros of SYSH are either inside the unit
//	circle or at infinity. The algorithm computes a discrete-time
//	state-space system SMP with transfer function M(z) and zeros only inside
//	the unit circle and a inner FIR system A(z)=A0+A1/z+...+Adeg/z^deg of
//	degree deg<=L such that H(z)=M(z)*A(z). Here, note that M(z) has a
//	delay-free stable left inverse and inner means that A(z)*A(1/z')'=I. The
//	routine returns SMP and A in state-space form. It also returns the
//	state-space system Atilde, which is a realization of A(1/z')'/z^deg.
//
// Algorithm:
//
//	Special form of Silvermans Structure Algorithm; see, e.g., Silverman:
//	"Inversion of Multivariable Linear Systems", IEEE Trans. Autom. Control,
//	Vol. 14, No. 3, 1969
//
// Authors:
//
//	Sander WAHLS
//

  // initialize algorithm
  [q,p] = size(SYSH.D);
  SMP = SYSH;
  deg = 0;
  A = syslin("d",[],[],[],eye(p,p));
  delay = syslin("d",0,1,1,0);

  // compute SMP and A
  [U,S,V,rk] = svd(SMP.D);
  while rk<p do
    SMP = SMP*V;
    SMP = syslin("d",SMP.A,[SMP.B(:,1:rk) SMP.A*SMP.B(:,rk+1:$)],SMP.C, ...
      [SMP.D(:,1:rk) SMP.C*SMP.B(:,rk+1:$)]);
    A = [eye(rk,rk) zeros(rk,p-rk) ; zeros(p-rk,rk) delay*eye(p-rk,p-rk)]*V'*A;
    deg = deg+1;
    if deg>L then
      error(sprintf("Could not factorize"));
    end
    [U,S,V,rk] = svd(SMP.D);
  end

  // compute Atilde
  Atilde = syslin("d",[],[],[],A.D');
  if deg>=1 then
    Atilde = (delay*eye(p,p))*Atilde + A.B'*A.C';
  end
  for k=2:deg do
    Atilde = (delay*eye(p,p))*Atilde + A.B'*A.A'^(k-1)*A.C';
  end

endfunction