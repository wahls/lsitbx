function Z = cplxlyap(X,Y)
//
// Purpose:
//
//	Solves real and complex discrete-time Lyapunov equations.
//
// Description:
//
//	The routine solves the discrete-time Lyapunov equation X'*Z*X-Z=Y
//	for Z. The matrices X and Y should be square, Y should additionally
//	be hermitian. We assume that the absolute values of the eigenvalues
//	of X are lower than one.
//
// Authors:
//
//	Sander WAHLS
//

  if isreal(X)&isreal(Y) then	// use Scilabs lyap function for the real case
    Z = lyap(X,Y,"d");
  else				// complex case is not supported by lyap
				// => we reduce it to the real case
    XR = real(X);
    XI = imag(X);
    YR = real(Y);
    YI = imag(Y);
    Z = lyap([XR -XI;XI XR],[YR -YI;YI YR],"d");
    n = size(X,1);
    Z = Z(1:n,1:n)+%i*Z(n+1:$,1:n);
  end

endfunction