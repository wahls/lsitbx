function [SYSRc,SYSRac] = parapinv(SYSH)
//
// Purpose:
//
//	Computes the causal and the anti-causal part of the para-pseudoinverse.
//
// Description:
//
//	We consider the discrete-time state-space system SYSH with transfer
//	function H(z). We assume that H(z) has full rank on the unit circle.
//	The routine computes stable state-space realizations SYSRc and SYSRac
//	for the causal and the anti-causal part of the para-pseudoinverse
//	R(z)=inv(H(1/z')'*H(z))*H(1/z')' (or, R(z)=H(1/z')'*inv(H(z)*H(1/z')')
//	if H is wide). In other words, if Rc(z) and Rac(z) denote the transfer
//	functions of SYSRc and SYSRac, then R(z)=Rc(z)+Rac(1/z).
//
// Algorithm:
//
//	See Chai, Zhang, Zhang, Mosca: "Optimal Noise Reduction in Oversampled
//	PR Filter Banks", IEEE Trans. Signal Process., vol. 57, no. 10, 2009
//
// Authors:
//
//	Sander WAHLS
//

  // check inputs
  if argn(2)~=1 then
    error("Wrong number of arguments.");
  end
  if typeof(SYSH)~="state-space" then
    error("SYSH is no state-space system.");
  end
  if typeof(SYSH.D)~="constant" then
    error("SYSH.D is no constant.");
  end

  // transpose input if necessary (formulae are for non-fat systems)
  [q,p] = size(SYSH.D);
  transposed = q<p;
  if transposed==%T then
    SYSH = SYSH';
  end

  // compute para-pseudoinverse
  [A,B,C,D] = abcd(SYSH);
  [X,F] = dare(A,B,D'*D,C'*D,C'*C);
  P = D'*D+B'*X*B;
  Y = cplxlyap((A+B*F)',-B*inv(P)*B');
  SYSRc = syslin("d",A+B*F,(A+B*F)*Y*(C+D*F)'+B*inv(P)*D',F,inv(P)*D'+ ...
    F*Y*(C+D*F)');
  SYSRac = syslin("d",(A+B*F)',(C+D*F)',inv(P)*B'+F*Y*(A+B*F)');

  // transpose output if input has been transposed
  if transposed==%T then
    SYSRc = SYSRc';
    SYSRac = SYSRac';
  end

endfunction