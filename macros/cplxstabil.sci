function K = cplxstabil(A,B,dt)
//
// Purpose:
//
//	Computes a stabilizing gain.
//
// Description:
//
//	We consider matrices A and B with A square and assume that the pair
//	(A,B) is stabilizable. The routine finds a matrix K such that A+B*K is
//	dt-stable where dt="d" in the discrete-time case and dt="c" in the
//	continuous-time case. If dt="d", this means that all eigenvalues of
//	A+B*K have an absolute value lower than one. If dt="c", this means that
//	all eigenvalues have real parts lower than zero.
//
// Authors:
//
//	Sander WAHLS
//

  [n,m] = size(A);
  l = size(B,2);

  if dt=="d" then			// discrete-time system

    if max(abs(spec(A)))<0.999 then	// catch trivial case A stable
      K = zeros(B)';
    elseif (n==1)&(m==1) then		// catch trivial SISO case, which
					// sometimes troubles ppol...
      K = -pinv(B)*A;			
      if abs(A+B*K)>=1 then
	error("(A,B) is not stabilizable");
      end
    else				// no trivial solution
      // compute stabilizable form of (A,B)
      [ns,nc,U] = st_ility(syslin('d',A,B,[]));
      if ns==n then			// (A,B) is stabilizable
	An = U'*A*U;
	Bn = U'*B;
	if isreal(A)&isreal(B) then	// use ppol if both A and B are real
	  try
	    K = -ppol(An(1:nc,1:nc),Bn(1:nc,:),zeros(1,nc));
	  catch
	    error(sprintf("(A,B) is stabilizable but pole placement failed" ...
	      +" (ppol error: ""%s"")",lasterror()));
	  end
	else				// try to use a stabilizing Riccati
					//  solution if A and B are complex
	  ln = size(Bn,2);
	  scale = 1e-6*max([max(abs(An)) max(abs(Bn))]);
	  [X,K] = dare(An(1:nc,1:nc),Bn(1:nc,:),scale*eye(ln,ln), ...
	    zeros(nc,ln),scale*eye(nc,nc));
	  if max(abs(spec(An(1:nc,1:nc)+Bn(1:nc,:)*K)))>=1 then
	    error("(A,B) is stabilizable but the algorithm failed");
	  end
	end
	K = [K zeros(l,n-nc)]*U';
      else				// stabilizable subspace computed by
					// st_ility is smaller than n; normally
					// this means that (A,B) is not
					// stabilizable, but sometimes it is a
					// false alarm => we try the riccati
					// approach which also works without the
					// stabilizable form
	scale = 1e-6*max([max(abs(A)) max(abs(B))]);
	try
	  [X,K] = dare(A,B,scale*eye(l,l),zeros(n,l),scale*eye(n,n));
	catch
	  error("(A,B) is not stabilizable");
	end
      end
    end

  elseif dt=="c" then			// continuous-time system

    if max(real(spec(A)))<0 then	// catch trivial case A stable
      K = zeros(B)';
    elseif (n==1)&(m==1) then		// catch trivial SISO case, which
					// sometimes troubles ppol...
      K = -pinv(B)*(A+1);
      if real(A+B*K)>=0 then
	error("(A,B) is not stabilizable");
      end
    else				// no trivial solution
      [ns,nc,U] = st_ility(syslin('c',A,B,[]));
      if ns==n then			// (A,B) is stabilizable
	An = U'*A*U;
	Bn = U'*B;
	if isreal(A)&isreal(B) then	// use ppol if both A and B are real
	  try
	    K = -ppol(An(1:nc,1:nc),Bn(1:nc,:),-ones(1,nc));
	  catch
	    error(sprintf("(A,B) is stabilizable but pole placement failed" ...
	      +" (ppol error: ""%s"")",lasterror()));
	  end
	else				// try to use a stabilizing Riccati
					// solution if A and B are complex
	  X = riccati(An(1:nc,1:nc),Bn(1:nc,:)*Bn(1:nc,:)',eye(nc,nc),"c");
	  K = -Bn(1:nc,:)'*X;
	  if max(real(spec(An(1:nc,1:nc)+Bn(1:nc,:)*K)))>=0 then
	    error("(A,B) is stabilizable but the algorithm failed");
	  end
	end
	K = [K zeros(l,n-nc)]*U';
      else				// stabilizable subspace computed by
					// st_ility is smaller than n; normally
					// this means that (A,B) is not
					// stabilizable, but sometimes it is a
					// false alarm => we try the riccati
					// approach which also works without the
					// stabilizable form
	X = riccati(A,B*B',eye(n,n),"c");
	K = -B'*X;
	if max(real(spec(A+B*K)))>=0 then
	  error("(A,B) is not stabilizable");
	end
      end
    end

  else
    error("Unknown time domain dt");
  end

endfunction