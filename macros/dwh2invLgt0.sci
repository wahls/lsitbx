function [SYSG,varargout] = dwh2invLgt0(SYSH,SYSW,L)
//
// Purpose:
//
//	Computes stable discrete-time left inverses with minimal weighted
//	H2 norm (and decision delay).
//
// Description:
//
//	We consider the discrete-time state-space systems SYSH and SYSW with
//	transfer functions H(z) and W(z). The decision delay L is a natural
//	number. We assume that H(z) is tall with full normal rank and that the
//	zeros of SYSH are located inside the open unit disc. We furthermore
//	assume that SYSW is stable, that [I-H(z)*pinv(H(z))]*W(z) has constant
//	rank on the unit circle and that L>0. The algorithm computes a stable
//	discrete-time state-space system SYSG with transfer function G(z) such
//	that G(z)H(z)=I/z^L and the weighted H2 norm ||G*W|| is minimal.
//	Optionally, the routine can also compute a parametrization for the
//	complete set of optimal inverses (see the help for details).
//
// Algorithm:
//
//	See S. Wahls, H. Boche: "Novel System Inversion Algorithm with
//	Application to Perfect Reconstruction Filter Banks", IEEE Trans.
//	Signal Process., vol. 58, no. 6, pp. 3008-3016, june 2010
//
// Authors:
//
//	Sander WAHLS
//

  // extract system matrices
  [A,B,C,D] = abcd(SYSH);
  [q,p] = size(D);
  n = size(A,1);

  // extract weight matrices
  [AW,BW,CW,DW] = abcd(SYSW);
  nW = size(AW,1);
  [qW,pW] = size(DW);

  // construct Dplus and Dperp
  Dplus = pinv(D);
  [U,S1,V] = svd(eye(q,q)-D*Dplus);
  Dperp = U(:,1:q-p);

  // compute Bperp such that A0=A-B*Dplus*C-Bperp*Dperp'*C is stable
  TMP1 = A-B*Dplus*C;
  TMP2 = -Dperp'*C;
  Bperp = cplxstabil(TMP1',TMP2',"d")';

  // construct A0,B0,A0W,B0W,C0W
  A0 = TMP1+Bperp*TMP2;
  B0 = -B*Dplus-Bperp*Dperp';
  A0W = [AW zeros(nW,n) ; B0*CW A0];
  B0W = [BW ; B0*DW];
  C0W = [CW C];

  // Y11 and L1 via Proposition 3
  try
    [Y11,L1] = dare(A0W',C0W'*Dperp,Dperp'*DW*DW'*Dperp,B0W*DW'*Dperp,B0W*B0W');
  catch
    warning("DARE has no (unique) stabilizing solution. Trying to " ...
      +"reqularize with a small perturbation.");
    epsilon = 1e-12*max([max(abs(A0W)) max(abs(C0W'*Dperp)) ...
      max(abs(Dperp'*DW*DW'*Dperp)) max(abs(B0W*DW'*Dperp)) ...
      max(abs(B0W*B0W'))]);
    [Y11,L1] = dare(A0W',C0W'*Dperp,Dperp'*DW*DW'*Dperp,B0W*DW'*Dperp, ...
      B0W*B0W'+epsilon*eye(n+nW,n+nW));
  end

  // Y12 via Proposition 3
  yk = (A0W*Y11*C0W'+B0W*DW'+L1'*Dperp'*(DW*DW'+C0W*Y11*C0W'))*Dplus';
  Y12 = [yk zeros(n+nW,(L-1)*p)];
  for k = 2:L do
    yk = (A0W+L1'*Dperp'*C0W)*yk;
    Y12(:,(k-1)*p+1:k*p) = yk;
  end
  
  // L and L0 via Proposition 4
  P = Dperp'*(DW*DW'+C0W*Y11*C0W')*Dperp;
  if or(P~=0) then
    L0W = -P\Dperp'*C0W*Y12(:,(L-1)*p+1:$);
    L2 = -P\Dperp'*[(C0W*Y11*C0W'+DW*DW')*Dplus' C0W*Y12(:,1:(L-1)*p)];
  else
    L0W = zeros(q-p,p);
    L2 = zeros(q-p,L*p);
  end
  LW = [L1 L2];

  // create optimal inverse
  Ahat = zeros(n+L*p,n+L*p);
  Ahat(1:n,1:n) = A0;
  Ahat(n+1:n+p,1:n) = Dplus*C;
  Ahat(n+p+1:n+L*p,n+1:n+(L-1)*p) = eye((L-1)*p,(L-1)*p);
  Bhat = zeros(n+L*p,q);
  Bhat(1:n,:) = B0;
  Bhat(n+1:n+p,:) = Dplus;
  AhatW = [AW zeros(nW,n+L*p) ; Bhat*CW Ahat];
  Ainv = AhatW+[LW'*Dperp'*C0W zeros(n+nW+L*p,L*p)];
  Binv = [zeros(nW,q);Bhat]+LW'*Dperp';
  Cinv = [L0W'*Dperp'*C0W zeros(p,(L-1)*p) eye(p,p)];
  Dinv = L0W'*Dperp';
  x0inv = [zeros(nW,1);-SYSH.x0;zeros(L*p,1)];
  SYSG = syslin("d",Ainv,Binv,Cinv,Dinv,x0inv);

  // note: the given inverse is different from the one given in the literature
  // reference above
  //
  // the inverse in the reference is
  //
  // A1 = [Ahat zeros(...) ; LW'*Dperp'*C zeros(...)
  // 				(AhatW+[LW'*Dperp'*C0W zeros(...)])]
  // B1 = [Bhat ; LW'*Dperp']
  // C1 = [L0W'*Dperp'*C zeros(...) eye(p,p) L0W'*Dperp'*C0W zeros(...)
  //				eye(p,p)]
  // D1 = L0W'*Dperp';
  //
  // the slightly larger realization 
  //
  // A2 = [AhatW zeros(...) ; LW'*Dperp'*C0W zeros(...)
  //				(AhatW+[LW'*Dperp'*C0W zeros(...)])]
  // B2 = [zeros(...) ; Bhat ; LW'*Dperp']
  // C2 = [L0W'*Dperp'*C0W zeros(...) eye(p,p) L0W'*Dperp'*C0W zeros(...)
  //				eye(p,p)]
  // D2 = D1
  //
  // has the same transfer function; the realization we use can be obtained with
  // the transformation matrix T=[eye(...) zeros(...);eye(...) eye(...)], i.e.,
  // from considering
  //
  // A3 = T*A2*inv(T), B3=T*B2, C3=C2*inv(T), D3=D2
  //
  // and dropping the non-observable parts

  // if requested, return parametrization for the set of optimal solutions 
  if argn(1)>1 then

    // Gopt(z) is a optimal left inverse if and only ifGopt(z) = A(z)+K(z)*B(z)
    // where A(z)=G0(z)/z^L+(C11(z)+L0W'*C21(z))*G0perp(z) and
    // B(z)=P012*C21(z)*G0perp(z) and K(z) is the stable+causal free parameter.

    // compute G0(z) and G0perp(z)
    SYSG0 = syslin("d",A0,B0,Dplus*C,Dplus);
    SYSG0perp = syslin("d",A0,B0,Dperp'*C,Dperp');

    // compute rk, the rank of P
    [U,S,V] = svd(P);
    epsilon = 1e-8*max([max(abs(A0W)) max(abs(C0W'*Dperp)) ...
      max(abs(Dperp'*DW*DW'*Dperp)) max(abs(B0W*DW'*Dperp)) ...
      max(abs(B0W*B0W'))]);
    if max(abs(S))>epsilon then
      rk = sum(diag(S) > 1e-8*max(diag(S)));
    else
      rk = 0;
    end

    // compute basis P012 for the coimage of P^0.5
    if rk<q-p then
      P012 = U(:,rk+1:$)';
    else
      P012 = zeros(1,q-p);
    end

    // construct C11(z) and C12(z)
    SYSC11 = syslin("d",AhatW+[LW'*Dperp'*C0W zeros(n+nW+L*p,L*p)],LW', ...
      [zeros(p,n+nW+(L-1)*p) eye(p,p)],zeros(p,size(LW,1)));
    SYSC21 = syslin("d",AhatW+[LW'*Dperp'*C0W zeros(n+nW+L*p,L*p)],LW', ...
      [Dperp'*C0W zeros(q-p,L*p)],eye(q-p,q-p));

    // construct A(z) and B(z)
    SYSA = add_delay(SYSG0,L)+(SYSC11+L0W'*SYSC21)*SYSG0perp;
    SYSB = P012*SYSC21*SYSG0perp;

    // add A(z) and B(z) to output list
    varargout = list(SYSA,SYSB);
  else
    varargout = list();
  end

endfunction
