function [SYSG,varargout] = dwh2invLeq0(SYSH,SYSW)
//
// Purpose:
//
//	Computes stable discrete-time left inverses with minimal weighted
//	H2 norm (delay-free case).
//
// Description:
//
//	We consider the discrete-time state-space systems SYSH and SYSW with
//	transfer functions H(z) and W(z). We assume that H(z) is tall with full
//	normal rank and that all zeros of SYSH are located inside the open unit
//	disc. We furthermore assume that SYSW is stable and that
//	[I-H(z)*pinv(H(z))]*W(z) has constant rank on the unit circle. The
//	algorithm computes a stable discrete-time state-space system SYSG with
//	transfer function G(z) such that G(z)H(z)=I and the weighted H2 norm
//	||G*W|| is minimal. Optionally, the routine can also compute a
//	parametrization for the complete set of optimal inverses  (see the help
//	for details).
//
// Algorithm:
//
//	Modification of the algorithm in S. Wahls, H. Boche: "Novel System
//	Inversion Algorithm with Application to Perfect Reconstruction Filter
//	Banks", IEEE Trans. Signal Process., vol. 58, no. 6, pp. 3008-3016,
//	june 2010
//
// Authors:
//
//	Sander WAHLS
//

  // extract system matrices and their sizes
  [A,B,C,D] = abcd(SYSH);
  [q,p] = size(D);
  n = size(A,1);

  // extract weight matrices
  [AW,BW,CW,DW] = abcd(SYSW);
  nW = size(AW,1);
  [qW,pW] = size(DW);

  // construct Dplus and Dperp
  Dplus = pinv(D);
  [U,S1,V] = svd(eye(q,q)-D*Dplus);
  Dperp = U(:,1:q-p);

  // compute Bperp such that A0=A-B*Dplus*C-Bperp*Dperp'*C is stable
  TMP1 = A-B*Dplus*C;
  TMP2 = -Dperp'*C;
  Bperp = cplxstabil(TMP1',TMP2',"d")';

  // construct A0,B0,A0W,B0W,C0W
  A0 = TMP1+Bperp*TMP2;
  B0 = -B*Dplus-Bperp*Dperp';
  A0W = [AW zeros(nW,n) ; B0*CW A0];
  B0W = [BW ; B0*DW];
  C0W = [CW C];

  // solution to first full information control problem is trivial, X=0,
  F = -Dplus*C0W;
  F0 = -Dplus*DW;

  // solve second full information control problem
  try
    [Y,LW] = dare(A0W',C0W'*Dperp,Dperp'*DW*DW'*Dperp,B0W*DW'*Dperp,B0W*B0W');
  catch
    warning("DARE has no (unique) stabilizing solution. Trying to " ...
      +"reqularize with a small perturbation.");
    epsilon = 1e-12*max([max(abs(A0W)) max(abs(C0W'*Dperp)) ...
      max(abs(Dperp'*DW*DW'*Dperp)) max(abs(B0W*DW'*Dperp)) ...
      max(abs(B0W*B0W'))]);
    [Y,LW] = dare(A0W',C0W'*Dperp,Dperp'*DW*DW'*Dperp,B0W*DW'*Dperp, ...
      B0W*B0W'+epsilon*eye(n+nW,n+nW));
  end
  P = Dperp'*(DW*DW' + C0W*Y*C0W')*Dperp;
  if or(P~=0) then
    L0W = -P\(Dperp'*C0W*Y*C0W'*Dplus'+Dperp'*DW*DW'*Dplus');
  else
    L0W = zeros(q-p,p);
  end

  // compute optimal inverse; note that the dimensions of the inverse have been
  // reduced like in the weighted h2 inversion algorithm with decision delay;
  // cf. the comment in the file dwh2invLgt0.sci
  Ainv = A0W+LW'*Dperp'*C0W;
  Binv = [zeros(nW,q);B0]+LW'*Dperp';
  Cinv = L0W'*Dperp'*C0W-F;
  Dinv = Dplus+L0W'*Dperp';
  x0inv = [zeros(nW,1);-SYSH.x0];
  SYSG = syslin("d",Ainv,Binv,Cinv,Dinv,x0inv);

  // if requested, return parametrization for the set of optimal solutions 
  if argn(1)>1 then

    // Gopt(z) is a optimal left inverse if and only if Gopt(z)=A(z)+K(z)*B(z)
    // where A(z)=G0(z)+(C11(z)+L0W'*C21(z))*G0perp(z) and
    // B(z)=K(z)*P012*C21(z)*G0perp(z) and K(z) is the stable+causal free
    // parameter.

    // compute G0(z) and G0perp(z)
    SYSG0 = syslin("d",A0,B0,Dplus*C,Dplus);
    SYSG0perp = syslin("d",A0,B0,Dperp'*C,Dperp');
    
    // compute rk, the rank of P
    [U,S,V] = svd(P);
    epsilon = 1e-8*max([max(abs(A0W)) max(abs(C0W'*Dperp)) ...
      max(abs(Dperp'*DW*DW'*Dperp)) ...
      max(abs(B0W*DW'*Dperp)) max(abs(B0W*B0W'))]);
    if max(diag(S))>epsilon then
      rk = sum(diag(S) > 1e-8*max(diag(S)));
    else
      rk = 0;
    end

    // compute basis P012 for the coimage of P^0.5
    if rk<q-p then
      P012 = U(:,rk+1:$)';
    else
      P012 = zeros(1,q-p);
    end

    // construct C11(z) and C12(z)
    SYSC11 = syslin("d",A0W+LW'*Dperp'*C0W,LW',Dplus*C0W,zeros(p,size(LW',2)));
    SYSC21 = syslin("d",A0W+LW'*Dperp'*C0W,LW',Dperp'*C0W,eye(q-p,q-p));

    // construct A(z) and B(z)
    SYSA = SYSG0+(SYSC11+L0W'*SYSC21)*SYSG0perp;
    SYSB = P012*SYSC21*SYSG0perp;

    // add A(z) and B(z) to output list
    varargout = list(SYSA,SYSB);
  else
    varargout = list();
  end

endfunction