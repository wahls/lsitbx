function val = dh2norm(SYS)
//
// Purpose:
//
//	Computes the H2 norm of a stable discrete-time state-space system.
//
// Description:
//
//	Let H(z) denote the transfer function of SYS. The routine computes the
//	H2 norm ||H||^2=[integral trace(H(exp(%i*f))*H(exp(%i*f))') df]/(2*%pi)
//	where f ranges from 0 to 2*%pi.
//
// Algorithm:
//
//	See Zhou, K.; Doyle, J. & Glover, K., "Robust and Optimal Control",
//	Prentice-Hall, 1996, Chapter 21.5
//
// Authors:
//
//	Sander WAHLS
//

  // check inputs
  if typeof(SYS)~="state-space" then
    error("SYS is no state-space system");
  end
  if typeof(SYS.D)~="constant" then
    error("SYS.D is no constant");
  end
  if SYS.dt~="d" then
    error("SYS is no discrete-time system");
  end

  // compute gramian
  [A,B,C,D] = abcd(SYS);
  X = cplxlyap(A,-C'*C);
  
  // compute norm
  val = real(sqrt(trace(B'*X*B+D'*D)));

endfunction
