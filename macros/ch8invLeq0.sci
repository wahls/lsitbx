function SYSG = ch8invLeq0(SYSH,tol,test_tol,verbose)
//
// Purpose:
//
//	Computes stable continuous-time left inverses with approximately
//	infimal H-infinity norm.
//
// Description:
//
//	We consider the continuous-time state-space system SYSH with transfer
//	function H(s). We assume that H(s) is tall with full normal rank and
//	that all zeros of SYSH are located in the open left half plane. The
//	algorithm computes a stable continuous-time state-space system SYSG with
//	transfer function G(s) such that G(s)H(s)=I and the H-infinity norm of
//	G(s) is close to infimal. The scalar tol>0 regulates how close the norm
//	of G(s) should be to the infimal value. Note: A larger value means a
//	longer run-time. Too small values may result in numerical problems. The
//	scalar test_tol regulates the tolerance in some internal tests. The
//	boolean verbose determines whether status messages should be displayed
//	while the algorithm is running.
//
// Algorithm:
//
//	The problem is reformulated as a full information control problem
//	which then is solved with ch8fic(). See Li, L. & Gu, G., "Design of
//	optimal zero-forcing precoders for MIMO channels via optimal full
//	information control", IEEE Trans. Signal Process., 2005, vol. 53,
//	pp. 3238-3246
//
// Authors:
//
//	Sander WAHLS
//

  // the used algorithm is for right inversion, so we transpose SYS
  SYSH = SYSH';
  
  // extract system matrices and their sizes
  [A,B,C,D] = abcd(SYSH);
  [q,p] = size(D);
  n = size(A,1);

  // construct Dplus and Dperp
  Dplus = pinv(D);
  [U,S1,V] = svd(eye(p,p)-Dplus*D);
  Dperp = U(:,1:p-q)';

  A0 = A-B*Dplus*C;
  B0 = B*Dperp';

  // compute upper bound on the optimal H-infinity norm
  // by using an arbitrary stable inverse
  F = cplxstabil(A0,B0,"c");
  ub = (1+tol)*h_norm(syslin("c",A0+B0*F,B*Dplus,Dperp'*F-Dplus*C,Dplus))+2*tol;
  N = ceil(log2(ub/tol));

  // solve full information problem
  [Kopt,Qopt,nrm] = ch8fic(A0,B*Dplus,B0,-Dplus*C,Dplus,Dperp',0,ub,N, ...
    test_tol,verbose);

  // construct optimal right inverse for transpose_syslin(SYS)
  Ainv = A0+B0*Kopt;
  Binv = B*Dplus+B0*Qopt;
  Cinv = Dperp'*Kopt-Dplus*C;
  Dinv = Dplus+Dperp'*Qopt;
  x0inv = -SYSH.x0;
  SYSG = syslin("c",Ainv,Binv,Cinv,Dinv,x0inv);

  // transpose again to obtain optimal left inverse
  SYSG = SYSG';
  
endfunction
