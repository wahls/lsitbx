function SYSD = add_delay(SYS,L)
//
// Purpose:
//
//	Adds output delay to a discrete-time state-space system.
//
//
// Description:
//
//	Suppose that SYS is a discrete-time state-space system with transfer
//	function H(z) and that L is a natural number. Then, SYSD is a
//	discrete-time state-space system with transfer function H(z)/z^L.
//
// Authors:
//
//	Sander WAHLS
//

  // check arguments
  if argn(2)~=2 then
    error("Wrong number of arguments, see help");
  end
  if typeof(SYS)~="state-space" then
    error("SYS is no state-space system");
  end
  if typeof(SYS.D)~="constant" then
    error("SYS.D is no constant");
  end
  try
    isreal(L);
  catch
    error("L is not real");
  end
  if size(L,"*")~=1 then
    error("L is no scalar");
  end
  if L<0 then
    error("L is negative");
  end
  if L~=round(L) then
    error("L is no integer");
  end  

  if L==0 then		// delay is zero => nothing to do
    SYSD = SYS;
  else			// delay is larger than zero
    [A,B,C,D] = abcd(SYS);
    n = size(A,1);
    [q,p] = size(D);
    Ad = zeros(n+L*q,n+L*q);
    Ad(1:n,1:n) = A;
    Ad(n+1:n+q,1:n) = C;
    Ad(n+q+1:n+L*q,n+1:n+(L-1)*q) = eye((L-1)*q,(L-1)*q);
    Bd = zeros(n+L*q,p);
    Bd(1:n,:) = B;
    Bd(n+1:n+q,:) = D;   
    Cd = zeros(q,n+L*q);
    Cd(:,n+(L-1)*q+1:$) = eye(q,q);
    Dd = zeros(D);
    x0d = zeros(n+L*q,1);
    x0d(1:n) = SYS.x0;
    SYSD = syslin(SYS.dt,Ad,Bd,Cd,Dd,x0d);
  end
  
endfunction

