function [X,F] = dare(A,B,R,L,Q)
//
// Purpose:
//
//	Finds the stabilizing solution to the discrete-time algebraic
//	Riccati equation.
//
// Description:
//
//	The routine tries to find the stabilizing solution X=X' to the
//	discrete-time algebraic Riccati equation
//	A'*X*A-X-(L+A'*X*B)*inv(R+B'*X*B)*(B'*X*A+L')+Q=0. Here, stabilizing
//	means that the eigenvalues of A+B*F, where F=-inv(R+B'*X*B)*(B'*X*A+L'),
//	have absolute values lower than one. We assume that such a solution
//	exits. Note: The routine also works if R is singular.
//
// Algorithm:
//
//	The initial guess is computed as described in V. Ionescu, M. Weiss:
//	"On computing the stabilizing solution of the discrete-time Riccati
//	equation", Linear Algebra Appl., 1992, vol. 174, pp. 229-238. The
//	initial guess is refined using the Newton iteration (without
//	acceleration) similar to P. Benner, "Accelerating Newton's Method for
//	Discrete-Time Algebraic Riccati Equations", Proc. MTNS 1998
//
// Authors:
//
//	Sander WAHLS
//

  // define tolerance
  tol = 1e-6;

  // check inputs
  [n,m] = size(B);
  if or(size(A)~=[n,n]) then
    error("Sizes of A and B are not compatible.");
  end
  if or(size(R)~=[m,m]) then
    error("Sizes of R and B are not compatible.");
  end
  if or(size(L)~=[n,m]) then
    error("Sizes of L and B are not compatible.");
  end
  if or(size(Q)~=[n,n]) then
    error("Sizes of Q and B are not compatible.");
  end
  if max(abs(R-R'))>tol then
    error("R is not hermitian.");
  end
  if max(abs(Q-Q'))>tol then
    error("Q is not hermitian.");
  end

  // construct matrix pencil
  M = [eye(n,n) zeros(n,n+m) ; zeros(n,n) -A' zeros(n,m) ; zeros(m,n) -B' ...
    zeros(m,m)];
  N = [A zeros(n,n) B ; Q -eye(n,n) L ; L' zeros(m,n) R];

  // find schur form and compute DARE solution
  [Ms,Ns,Z,dim] = schur(N,M,"d");
  X = Z(n+1:2*n,1:n)/Z(1:n,1:n);
  F = Z(2*n+1:$,1:n)/Z(1:n,1:n);

  // perform three newton steps to improve DARE solution
  [X,F,res,rho] = dare_newton(A,B,R,L,Q,X,F,3);
  res = norm(A'*X*A-X-(L+A'*X*B)/(R+B'*X*B)*(B'*X*A+L')+Q,1);
  rho = max(abs(spec(A+B*F)));

  // compute scaling factor for the residual
  scale = max([max(abs(A)) max(abs(B)) max(abs(R)) max(abs(L)) max(abs(Q))]);

  // try to further improve DARE solution if necessary
  if (res>scale*tol)|(rho>=1) then
    warning("Bad DARE solution. Performing another 25 Newton iterations.");
    [X,F,res,rho] = dare_newton(A,B,R,L,Q,X,F,25);
  end

  // check if solution is ok
  if res>scale*tol then
    error(sprintf("Computed DARE solution has large residual, " ...
      +"||A''*X*A-X-(L+A''*X*B)/(R+B''*X*B)*(B''*X*A+L'')+Q||=%e.",res));
  end
  if rho>=1 then
    error(sprintf("DARE has no (unique) stabilizing solution, " ...
      +"max(abs(spec(A+B*F)))=%f.",rho));
  end

endfunction

function [X,F,res,rho] = dare_newton(A,B,R,L,Q,X,F,K)
// Improves an approximate solution (X,F) to the DARE given above by
// choosing the best solution among K Newton steps

  // initialize Newton iteration
  Xk = X;
  Fk = F;
  Rk = A'*X*A-X-(L+A'*X*B)/(R+B'*X*B)*(B'*X*A+L')+Q;
  res = norm(Rk,1);
  rho = max(abs(spec(A+B*F)));

  while K>0 do	// use "while" loop because strangely "for" triggers some
		// Scilab bug => tests/unit_tests/dh8fic.tst then fails

    // perform the Newton step
    Ak = A+B*Fk;
    Nk = cplxlyap(Ak,-Rk);
    Xk = Xk+Nk;
    Xk = 0.5*(Xk+Xk');
    Fk = -(R+B'*Xk*B)\(B'*Xk*A+L');
    Rk = A'*Xk*A-Xk-(L+A'*Xk*B)/(R+B'*Xk*B)*(B'*Xk*A+L')+Q;

    // compute residual and spectral radius of the closed-loop matrix
    rhok = max(abs(spec(Ak)));
    resk = norm(Rk,1);

    // if the residual is lower than for the last best solution, and the
    // solution is stabilizing, save the current solution as new best solution
    if (resk<=res)&(rhok<1) then
      X = Xk;
      F = Fk;
      res = resk;
      rho = rhok;
    end

    // decrease counter variable
    K = K-1;

  end

endfunction